package com.conygre.simple;

public class TestAccount2 {

    public static void main(String[] args) {
        Account[] arrayOfAccounts = new Account[5];
        //arrayOfAccounts[0] = new Account("Patrick",100);
        //arrayOfAccounts[1] = new Account("Spongebob", 345);
        //arrayOfAccounts[2] = new Account("Sandy",2000);
        //arrayOfAccounts[3] = new Account("Mr Krabs", 100000);
        //arrayOfAccounts[4] = new Account("Plankton",7654);

        Account.setInterestRate(1.2);
        double[] amounts = {0, 20, 432, 2000, 23345};
        String[] names = {"Patrick", "Spongebob", "Sandy", "Mr Krabs", "Squidward"};


        for (int i = 0; i < 5; i++) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);

            System.out.println(arrayOfAccounts[i].getName() + " has " + arrayOfAccounts[i].getBalance() + "in their account.");
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getBalance());
            System.out.println(Account.getInterestRate());
        }
        System.out.println("BEFORE" + arrayOfAccounts[3].getBalance());
        System.out.println(arrayOfAccounts[2].getBalance());
        arrayOfAccounts[3].withdraw(400);
        arrayOfAccounts[2].withdraw();
        System.out.println(arrayOfAccounts[3].getBalance());
        System.out.println(arrayOfAccounts[2].getBalance());

    }


}
