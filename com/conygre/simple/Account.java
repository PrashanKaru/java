package com.conygre.simple;

public class Account {

    private double balance;
    private String name;
    public static double interestRate;

    public Account(String name, double balance) {
        this.name = name;
        this.balance = balance;
    }

    public Account() {
        this("Prashan", 50);
    }

    public boolean withdraw(double amount) {
        if (this.balance < amount) {
            return false;
        }
        this.balance -= amount;
        return true;
    }


    public boolean withdraw() {
        return this.withdraw(20);
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addInterest() {
        //balance = balance * interestRate;
    }


}
