package com.conygre.simple;

public class TestStrings {

    public static void main(String[] args) {
        String String1 = "example.doc";
        String1 = "example.bak";
        String String2 = "example.pdf";

        int var = String1.compareTo(String2);
        System.out.println(var);
        if (var > 0) {
            System.out.println("String 1 is lexicographically greater than String 2.");
        } else if (var < 0) {
            System.out.println("String 2 is lexicographically greater than String 1.");
        }

        String testString = "the quick brown fox swallowed down the lazy chicken";
        String toFind = "ow";

        System.out.println(testString.contains(toFind));


    }

}


