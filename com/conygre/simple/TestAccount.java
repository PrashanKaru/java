package com.conygre.simple;

public class TestAccount {

    public static void main(String[] args) {

        Account myAccount = new Account();
        myAccount.setBalance(1000);
        myAccount.setName("Prashan");

        System.out.println(myAccount.getName() + " has " + myAccount.getBalance() + " in their account.");

        Account[] arrayOfAccounts = new Account[5];
        double[] amounts = {0, 20, 432, 2, 23345};
        String[] names = {"Patrick", "Spongebob", "Sandy", "Mr Krabs", "Squidward"};


        for (int i = 0; i < 5; i++) {
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setName(names[i]);
            arrayOfAccounts[i].setBalance(amounts[i]);

            System.out.println(arrayOfAccounts[i].getName() + " has " + arrayOfAccounts[i].getBalance() + "in their account.");
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getBalance());
        }
    }
}
