package com.conygre.simple;


public class HelloWorld {

    public static void main(String[] args) {

        System.out.println("Hello from my first java program!");

        String make = "BMW";
        String model = "3 Series";
        double engineSize = 5.0;
        byte gear = 5;
        short speed = (short) (gear * 20);

        System.out.println("The make is " + make);
        System.out.println("The model is " + model);
        System.out.println("The engine size is " + engineSize);
        System.out.printf("The Speed is " + speed);

        if (engineSize <= 1.3) {
            System.out.printf("You have a small engine.");
        }

        if (gear == 1) {
            System.out.printf("Your car should go 10mph.");
        } else if (gear == 2) {
            System.out.printf("Your car should go 20mph.");
        } else if (gear == 3) {
            System.out.printf("Your car should go 30mph.");
        } else if (gear == 4) {
            System.out.printf("Your car should go 40mph.");
        } else {
            System.out.printf("You have a fast car.");
        }


    }

}
